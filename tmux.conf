# tmux config
# Eryn Wells <eryn@erynwells.me>

set -g prefix C-f

# Don't destroy sessions that have no clients attached.
set -g exit-unattached off

set -g default-terminal "screen-256color"
set -g history-limit 10000

# Bells from everywhere
set -g bell-action any
set -g visual-bell off

# Pass xterm titles through
set -g set-titles on
set -g set-titles-string "#T"

# Use vi keys for copy mode
set -g mode-keys vi
set -g repeat-time 0

# No left status; right status bar is session name
set -g status-bg black
set -g status-left ""
set -g status-left-bg black
set -g status-left-fg brightblue
set -g status-left-attr none

set -g status-right "| #S "
set -g status-right-bg black
set -g status-right-fg brightblue
set -g status-right-attr none

# Start window and pane indexing from 1 instead of 0
set-option -g base-index 1
set-option -g pane-base-index 1

# Tabs like this: "(<index>:<window_name>)"
setw -g window-status-bg black
setw -g window-status-fg brightgreen
setw -g window-status-format "(#I:#W)"
setw -g window-status-current-bg black
setw -g window-status-current-fg yellow
setw -g window-status-current-format "(#I:#W)"

setw -g window-status-bell-fg red
setw -g window-status-content-fg brightred
setw -g window-status-activity-fg brightred

setw -g alternate-screen on

setw -g clock-mode-style 24

# Lock the screen after 120 seconds
set-option -g lock-after-time 0 
set-option -g lock-command "tmux clock-mode"

bind-key C-a last-window
